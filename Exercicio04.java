/*

4.  Elaborar  um  programa  que  efetue  a  apresentação  do  valor  da  conversão  em  real  (R$)  de  um 
valor  lido  em  dólar  (US$).  O  programa  deverá  solicitar  o  valor  da  cotação  do  dólar  e  também  a 
quantidade de dólares disponíveis com o usuário.

*/

import Lib.*;

public class Exercicio04 {
    public static void Executar(){

        double Cotacao = Prompt.lerDecimal("Digite a cotaçao do dolar:");

        double Dolar = Prompt.lerDecimal("Digite qual o valor em dolar voce deseja converter para real:");

        Conversao Conversao = new Conversao(Cotacao, Dolar);

        Prompt.imprimir("O valor total em real é de R$" + Conversao.real());
        
    }
}