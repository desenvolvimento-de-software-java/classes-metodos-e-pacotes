/*

23. Faça um programa que leia o nome, o sexo, a altura e a idade de uma pessoa. Calcule e mostre 
nome e o seu peso ideal de acordo com as seguintes características da pessoa: 
Sexo Altura Idade Peso Ideal:

*/

import Lib.*;

public class Exercicio23 {
    public static void Executar(){

        boolean Var;

        String Nome = Prompt.lerLinha("DIgite o nome");

        int Idade = Prompt.lerInteiro("Digite a sua idade");

        double Altura = Prompt.lerDecimal("Digite a sua altura");

        do {

            Var=false;

            String Sexo = Prompt.lerLinha("Digite o seu sexo");

            double Ideal = Peso.EMC(Altura, Idade, Sexo);

            if (Ideal!=0) {
                Prompt.imprimir("O peso ideal para " + Nome + " é " + Ideal);
            }
    
            else{
                Var=true;
            }
            
        } while (Var==true);
        
    }
}