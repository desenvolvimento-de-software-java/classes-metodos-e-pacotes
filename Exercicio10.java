/*

10. Faça um programa que receba a idade de um número finito de pessoas e mostre mensagem 
informando “maior de idade” e “menor de idade” para cada pessoa. Considerar a pessoa com 18 
anos como maior de idade.

*/

import Lib.*;

public class Exercicio10 {
    public static void Executar(){

        int Limit = Prompt.lerInteiro("Digite quantos pessoas voce que analizar: ");

        Intervalo Intervalo = new Intervalo(Limit);

        Intervalo.NomesEIdades();
        
    }
}