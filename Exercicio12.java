/*

12.  A  concessionária  de  veículos  “CARANGO  VELHO”  está  vendendo  os  seus  veículos  com 
desconto.  Faça  um  programa  que  calcule  e  exiba  o  valor  do  desconto  e  o valor  a  ser  pago  pelo 
cliente de vários carros. O desconto deverá ser calculado de acordo com o ano do veículo. Até 2000 
- 12% e acima de 2000 - 7%. O sistema deverá perguntar se deseja continuar calculando desconto 
até que a resposta seja: “(N) Não”. Informar total de carros com ano até 2000 e total geral.

*/

import Lib.*;

public class Exercicio12 {
    public static void Executar(){

        boolean Veri;
        double ValorFinal;
        int Total2, TotalG;

        Carango Carango = new Carango();

        do {

            Veri=false;

            Carango.Resultado Resultado = Carango.CalculoAno();

            ValorFinal = Resultado.getValorFinal();
            Total2 = Resultado.getTotal2();
            TotalG = Resultado.getTotalG();

            Prompt.imprimir("O preço do carro com o desconte sera de " + ValorFinal);

            Prompt.separador();

            String Var = Prompt.lerLinha("Deseja analizar outro carro?");

            Prompt.separador();

            Veri = Carango.Veri(Var);
            
        } while (Veri==true);

        Prompt.imprimir("Total de carros analizados = " + TotalG);
        Prompt.imprimir("Total de carros fabricados antes dos anos 2000 = " + Total2);
        
    }
}