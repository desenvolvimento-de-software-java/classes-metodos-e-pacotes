/*

19. Escrever um programa que leia três valores inteiros e verifique se eles podem ser os lados de 
um triângulo. Se forem, informar qual o tipo de triângulo que eles formam: equilátero, isóscele ou 
escaleno. 
    Propriedade:  o  comprimento  de  cada  lado  de  um  triângulo  é  menor  do  que  a  soma  dos 
    comprimentos dos outros dois lados. 
    Triângulo Equilátero: aquele que tem os comprimentos dos três lados iguais; 
    Triângulo  Isóscele: aquele  que  tem  os  comprimentos  de  dois  lados  iguais.  Portanto,  todo triângulo equilátero é também isóscele; 
    Triângulo Escaleno: aquele que tem os comprimentos de seus três lados diferentes.

*/

import Lib.*;

public class Exercicio19 {
    public static void Executar(){

        double LadoA = Prompt.lerDecimal("Digite a base do triangulo");
        double LadoB = Prompt.lerDecimal("Digite a base do triangulo");
        double LadoC = Prompt.lerDecimal("Digite a base do triangulo");

        Triangulo Triangulo = new Triangulo(LadoA, LadoB, LadoC);

        if (Triangulo.TrianguloVar()==false) {
            Prompt.imprimir("Os valore informados nao corresponde a um triangulo. ");
        } else {
            Prompt.imprimir("Os valore informados corresponde a um triangulo " + Triangulo.Tipo());
        }        
    }
}