/*

5.  A  Loja  Mamão  com  Açúcar  está  vendendo  seus  produtos  em  5  (cinco)  prestações  sem  juros. 
Faça um programa que receba um valor de uma compra e mostre o valor das prestações.

*/

import Lib.*;

public class Exercicio05 {
    public static void Executar(){

        double Valor = Prompt.lerDecimal("Digite o valor total da compra para ver os valores das parcelas");

        Prestacoes Prestacoes = new Prestacoes();

        Prestacoes.Parcelas(Valor);
        
    }
}