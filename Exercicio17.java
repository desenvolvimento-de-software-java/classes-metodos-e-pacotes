/*

17. Leia o nome do funcionário, seu salário e o valor do salário mínimo. Calcule o seu novo salário 
reajustado.  Escrever  o  nome  do  funcionário,  o  reajuste  e  seu  novo  salário.  Calcule  quanto  à 
empresa vai aumentar sua folha de pagamento. 

*/

import Lib.*;

public class Exercicio17 {
    public static void Executar(){
        double Ajuste=0;
        boolean Veri;
        String Var;

        String Nome = Prompt.lerLinha("Digite o nome do funcionario");

        double Salario = Prompt.lerDecimal("Digite o salari no funcionario");

        do {

            Veri=false;

            Prompt.separador();

            Prompt.imprimir("P - Porcentagem sobre o salario  /  A - Acrecimo de valor");
            Var = Prompt.lerLinha("Digite o tipo de acrescimo que deseja fazer");

            Prompt.separador();

            switch (Var.toLowerCase()) {
                case "p":
                Ajuste = Prompt.lerDecimal("Digite a porcentagem de acrescimo ao salario do funcionario");
                    break;

                case "a":
                Ajuste = Prompt.lerDecimal("Digite um valor para acrecentar ao salario do funcionario");
                    break;
                default:
                    Prompt.imprimir("O tipo de reajuste não é compativel com as opções");
                    Veri=true;
                    break;
            }
            
        } while (Veri==true);

        double Total = Empresa.AjusteNovo(Salario, Ajuste, Var);

        Prompt.imprimir("O funcionario " + Nome + " ira ter um reajunte no salario e pasara a receber " + Total);
        
    }
}