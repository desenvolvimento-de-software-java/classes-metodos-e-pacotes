/*

13. Escrever um programa que leia os dados de “N” pessoas (nome, sexo, idade e saúde) e informe 
se está apta ou não para cumprir o serviço militar obrigatório. Informe os totais.

*/

import Lib.*;

public class Exercicio13 {
    public static void Executar(){

        int Limite = Prompt.lerInteiro("Digite quantos candidatos deseja cadastrar");

        Exercito Exercito = new Exercito(Limite);

        Exercito.Candidatos();

        Exercito.CandidatoApto();
        
    }
}