package Lib;

public class Empresa {
    private double Vendas, Minimo;
    private String[] Nome, Sexo;
    private double[] SalarioBase, SalarioAjustado, Fixo, Total, Ajuste;
    private int[] Idade;
    private int Limite;

    public Empresa(int Limite){
        this.Limite = Limite;
        Nome = new String[Limite];
        Sexo = new String[Limite];
        SalarioBase = new double[Limite];
        SalarioAjustado = new double[Limite];
        Fixo = new double[Limite];
        Total = new double[Limite];
        Ajuste = new double[Limite];
        Idade = new int[Limite];
    }

    public static double AjusteNovo(double Salario, double Ajuste, String Var){

        switch (Var.toLowerCase()) {

            case "a":
                return Salario+Ajuste;

            case "p":
                return Salario+((Salario*Ajuste)/100);
        
            default:
                return 0;
        }

    }

    public void AjustSalario(){

        for (int i = 0; i < Limite; i++) {
            
            Nome[i] = Prompt.lerLinha("Digite o nome do funcionario:");

            Idade[i] = Prompt.lerInteiro("Digite a idade do funcionario:");

            Sexo Sexo_ = new Sexo();

            Sexo[i] = Sexo_.VeriSexo();
            
            Fixo[i] = Prompt.lerDecimal("Digite o valor do salario fixo do funcionario:");

        }

        for (int i = 0; i < Limite; i++) {

            if (Sexo[i].equals("Masculino")&&Idade[i]>=30) {
                Ajuste[i]=100;
            }

            else if(Sexo[i].equals("Masculino")&&Idade[i]<30) {
                Ajuste[i]=50;
            }

            else if(Sexo[i].equals("Feminino")&&Idade[i]>=30) {
                Ajuste[i]=200;
            }

            else if(Sexo[i].equals("Feminino")&&Idade[i]<30) {
                Ajuste[i]=80;
            }

            else{
                Ajuste[i]=0;
            }

            Total[i]=(Fixo[i]+Ajuste[i]);

        }

        for (int i = 0; i < Limite; i++) {
            Prompt.imprimir("O salario do funcionario " + Nome[i] + " com o abono fica um total de " + Total[i]);
        }

    }

    public void SalarioAjuste(){

        for (int i = 0; i < Limite; i++) {
            Nome[i] = Prompt.lerLinha("Digite o nome do funcionario:");
            Fixo[i] = Prompt.lerDecimal("Digite o valor do salario fixo do vendedor:");
            Vendas = Prompt.lerDecimal("Digite o valor total em vendas efetuados este mes:");

            Total[i]= Fixo[i] + ((Vendas*15)/100);

        }

        for (int i = 0; i < Fixo.length; i++) {
            Prompt.imprimir("O salario fixo do vendedor " + Nome[i] + " é de " + Fixo[i] + " e o salario total sera de " + Total[i]);
        }

    }

    public void Salario(){

        Minimo = Prompt.lerDecimal("Digite o valor do salario minimo atual: ");

        for (int i = 0; i < Limite; i++) {

            Nome[i] = Prompt.lerLinha("Digite o nome do funcionario:");

            SalarioBase[i] = Prompt.lerDecimal("Digite o valor atual do salario do funcionari");

            if (SalarioBase[i]<(3*Minimo)) {
                SalarioAjustado[i]+=SalarioBase[i]+((SalarioBase[i]*50)/100);
            }

            else if (SalarioBase[i]>(3*Minimo)||SalarioBase[i]<(10*Minimo)) {
                SalarioAjustado[i]+=SalarioBase[i]+((SalarioBase[i]*20)/100);
            }

            else if (SalarioBase[i]>(10*Minimo)||SalarioBase[i]<(20*Minimo)) {
                SalarioAjustado[i]+=SalarioBase[i]+((SalarioBase[i]*15)/100);
            }

            else{
                SalarioAjustado[i]+=SalarioBase[i]+((SalarioBase[i]*10)/100);
            }
            
        }

        Prompt.separador();

        for (int i = 0; i < Limite; i++) {
            Prompt.imprimir("O salario do funcionario " + Nome[i] + " antes do ajuste era de " + SalarioBase[i] + " e depois do ajuste é de " + SalarioAjustado[i]);
        }

    }

    public static Double SalarioProfessor(double Horas, int  Nivel){

        switch (Nivel) {
            case 1:
                return Horas*12;

            case 2:
            return Horas*17;

            case 3:
            return Horas*25;
        
            default:
                Prompt.imprimir("O nivel incorreto");
                Prompt.imprimir("O nivel aceitos 1, 2 e 3");
                return 0.0;
        }

    }

}