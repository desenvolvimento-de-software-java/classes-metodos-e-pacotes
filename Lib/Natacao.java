package Lib;

public class Natacao {

    public static String classificacao(int Idade){


        if (Idade>=5||Idade<=7) {
            return "Infantil A";
        } 
        
        else if (Idade>=8||Idade<=10){
            return "Infantil B";
        }

        else if (Idade>11||Idade<=13){
            return "juvenil A";
        }

        else if (Idade>=14||Idade<=17){
            return "juvenil B";
        }

        else if (Idade>=18||Idade<=25){
            return "Sênior";
        }

        else{
            return "Fora da classificação";
        }

    }

    public static boolean Veri(String Veri){

        switch (Veri.toLowerCase()) {
            case "v":
                return true;
            default:
                return false;
        }

    }
}