package Lib;

public class Grupo {

    public static int Risco(int Idade, String Tipo){

        if (Idade>=17&&Idade<=20) {
            
            switch (Tipo.toLowerCase()) {
                case "b":
                    return 1;
    
                case "m":
                    return 2;
    
                case "a":
                    return 3;
            
                default:
                    return 0;
            }
    
        }
    
        else if(Idade>=21&&Idade<=24){
    
            switch (Tipo.toLowerCase()) {
                case "b":
                    return 2;
    
                case "m":
                    return 3;
    
                case "a":
                    return 4;
            
                default:
                    return 0;
            }
    
        }
    
        else if(Idade>=25&&Idade<=34){
    
            switch (Tipo.toLowerCase()) {
                case "b":
                    return 3;
    
                case "m":
                    return 4;
    
                case "a":
                    return 5;
            
                default:
                    return 0;
            }
            
        }
    
        else if(Idade>=35&&Idade<=64){
    
            switch (Tipo.toLowerCase()) {
                case "b":
                    return 4;
    
                case "m":
                    return 5;
    
                case "a":
                    return 6;
            
                default:
                    return 0;
            }
            
        }
    
        else{
    
            switch (Tipo.toLowerCase()) {
                case "b":
                    return 7;
    
                case "m":
                    return 8;
    
                case "a":
                    return 9;
            
                default:
                    return 0;
            }
    
        }
    
    
    }
    
}