package Lib;

public class Exercito {
    private int Limite, TotalM=0, TotalF=0;
    private String[] Nome, Sexo, Saude, Apto;
    private int[] Idade;

    public Exercito(int Limite){
        this.Limite = Limite;
        Saude = new String[Limite];
        Nome = new String[Limite];
        Sexo = new String[Limite];
        Apto = new String[Limite];
        Idade = new int[Limite];
    }

    public void Candidatos(){

        Sexo Sexo_ = new Sexo();

        for (int i = 0; i < Limite; i++) {

        Nome[i] = Prompt.lerLinha("Digite o nome do candidato");

        Sexo[i] = Sexo_.VeriSexo();

        TotalM+=Sexo_.GetSexoM();
        TotalF+=Sexo_.GetSexoF();

        Saude[i] = Prompt.lerLinha("Digite a saude atual do candidato");

        Idade[i] = Prompt.lerInteiro("Digite a idade do candidato");

        if (Idade[i]>=18 && Sexo[i].equals("Masculino")) {
            Apto[i] = "Apto";
        }else{
            Apto[i] = "Inapto";
        }

        }
    }

    public void CandidatoApto(){

        for (int i = 0; i < Limite; i++) {

            Prompt.imprimir("O candidato(a) " + Nome[i] + " é " + Apto[i] + " a participar do exercito.");
            
        }

        Prompt.separador();

        Prompt.imprimir("O todal de candidatos Masculinos é " + TotalM);
        Prompt.imprimir("O todal de candidatos Feminino é " + TotalF);

    }
    
}