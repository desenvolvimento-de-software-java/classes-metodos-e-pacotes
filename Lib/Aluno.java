package Lib;

public class Aluno {
    private String Nome;
    private double Nota1, Nota2, Nota3, Media;

    public Aluno(String Nome, double Nota1, double Nota2, double Nota3) {
        this.Nome = Nome;
        this.Nota1 = Nota1;
        this.Nota2 = Nota2;
        this.Nota3 = Nota3;
    }

    public double calcularMedia() {
    return (Nota1 + Nota2 + Nota3) / 3;
    }

    public String getNome() {
        return Nome;
    }

    public static double VerificarNota(String nota){
        double Nota;
        do {
            Nota = Prompt.lerDecimal(nota);
            if (Nota < 0 || Nota > 10) {
                Prompt.imprimir("A nota informada não está dentro dos padrões");
            }
        } while (Nota < 0 || Nota > 10);
        return Nota;
    }

    public void menção() {
        Media=calcularMedia();

        if (Media>=6) {
            Prompt.imprimir("O aluno foi aprovado com media " + Media);
        } else {
            Prompt.imprimir("O aluno foi reprovado com media " + Media);
        }

    }

    public static double CalculoNota(Double Laboratorio, Double Avaliacao, Double Final){

        double Aux=0;

        Aux+=(Laboratorio*2);
        Aux+=(Avaliacao*3);
        Aux+=(Final*5);

        return Aux/10;

    }

    public static String Letra(Double Laboratorio, Double Avaliacao, Double Final){

        double Total = Aluno.CalculoNota(Laboratorio, Avaliacao, Final);

        if (Total>=0||Total<5) {
            return "R";
        } 
        
        else if (Total>=5||Total<6){
            return "D";
        }

        else if (Total>=6||Total<7){
            return "C";
        }

        else if (Total>=7||Total<8){
            return "B";
        }

        else {
            return "A";
        }

    }
    
}