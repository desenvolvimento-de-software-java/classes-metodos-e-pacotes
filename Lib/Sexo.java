package Lib;

public class Sexo {
    private boolean Var;
    private String Sexo;
    private int SexoM, SexoF;


    public String VeriSexo(){

        do{

            SexoM=0;
            SexoF=0;

            Var=false;

            Sexo = Prompt.lerLinha("Digite o sexo:");

            switch (Sexo.toLowerCase()) {
                case "m":
                    Sexo = "Masculino";
                    SexoM++;
                    break;
                case "f":
                    Sexo = "Feminino";
                    SexoF++;
                    break;

                default:
                    Prompt.imprimir("Sexo digitado nao condis com as opçoes. (M-Masculino, F-Feminino)");
                    Var=true;
                    break;
            }

        }while(Var==true);

        return Sexo;

    }

    public int GetSexoM(){
        return SexoM;
    }

    public int GetSexoF(){
        return SexoF;
    }

}