package Lib;

public class Carango {
    private boolean Var;
    private int Total2=0, TotalG=0;
    private double Desconto, ValorFinal, Total;

    public class Resultado{
        private int Total2, TotalG;
        private double ValorFinal;


        public Resultado (double ValorFinal, int Total2, int TotalG){
            this.ValorFinal=ValorFinal;
            this.Total2=Total2;
            this.TotalG=TotalG;
        }

        public double getValorFinal() {
            return ValorFinal;
        }

        public int getTotal2() {
            return Total2;
        }

        public int getTotalG() {
            return TotalG;
        }

    }

    public Resultado CalculoAno(){

        double Valor = Prompt.lerDecimal("Digite o valor do carro:");
        int Ano = Prompt.lerInteiro("Digite o ano do carro");

        if (Ano<2000) {
            Total2++;
            Desconto = Valor * (12.0/100);
        }else{            
            Desconto = Valor * (7.0/100);
        }

        ValorFinal = (Valor-Desconto);
        TotalG++;

        Resultado Resultado = new Resultado(ValorFinal, Total2, TotalG);

        return Resultado;

    }

    public boolean Veri(String Veri){

        do {

            Var=false;

            switch (Veri.toLowerCase()) {
                case "s":
                    return true;
                case "n":
                    return false;
    
                default:
                    Prompt.imprimir("A opçao digitada nao é valida (N - Não, S- Sim)");
                    Var=true;
                    break;
            }
            
        } while (Var);

        return false;

    }

    public void CalculoTipo(){
        double Valor = Prompt.lerDecimal("Digite o valor do carro: ");

        do {

            Var=false;

            String Tipo = Prompt.lerLinha("Digite o tipo de combustive que o carro utiliza: ");

            switch (Tipo) {
                case "alcool":
                    Desconto=(Valor*25)/100;
                    Total=Valor-Desconto;
                    break;
                    
                case "gasolina":
                    Desconto=(Valor*21)/100;
                    Total=Valor-Desconto;
                    break;

                case "diesel":
                    Desconto=(Valor*14)/100;
                    Total=Valor-Desconto;
                    break;

                default:
                    Prompt.imprimir("O combustivel digitado nao corresponde a um tipo existente");
                    Prompt.imprimir("alcool / gasolina / diesel");
                    Var=true;
                    break;
            }
            
        } while (Var==true);

        Prompt.imprimir("O valor do desconto é de " + Desconto);
        Prompt.imprimir("O valor do carro com o desconto é de " + Total);

    }
    
}