package Lib;

public class Conta { 

    public static double CalculoConta(int Tipo, double kwh){

        double Residencial = 0.60;
        double Comercial = 0.48;
        double Industrial = 1.29;

        switch (Tipo) {
            case 1:
                return kwh*Residencial;

            case 2:
                return kwh*Comercial;

            case 3:
                return kwh*Industrial;
        
            default:
                return 0;

        }

    }

}