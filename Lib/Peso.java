package Lib;

public class Peso {

    public static double EMC(double Altura, int Idade, String Sexo){

        switch (Sexo.toLowerCase()){

            case "m":
                return calcularPesoHomem(Altura, Idade);
            
            case "f":
                return calcularPesoMulher(Altura, Idade);

            default:
                return 0;

        }

    }

    private static double calcularPesoHomem(double Altura, int Idade){

        if (Altura > 1.70){

            if (Idade <= 20){

                return (72.7 * Altura) - 58;

            } 
            
            else if (Idade <= 39){

                return (72.7 * Altura) - 53;

            } 
            
            else{

                return (72.7 * Altura) - 45;

            }

        } 
        
        else{

            if (Idade <= 40){

                return (72.7 * Altura) - 50;

            } 
            
            else{

                return (72.7 * Altura) - 58;

            }

        }

    }

    private static double calcularPesoMulher(double Altura, int Idade){

        if (Altura > 1.50) {

            return (62.1 * Altura) - 44.7;

        } 
        
        else {

            if (Idade >= 35){

                return (62.1 * Altura) - 45;

            } 
            
            else{

                return (62.1 * Altura) - 49;

            }

        }

    }
}
