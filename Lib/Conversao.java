package Lib;

public class Conversao {

    private double Cotacao, Dolar;

    public Conversao (double Cotacao, double Dolar){
        this.Cotacao = Cotacao;
        this.Dolar = Dolar;
    }

    public double real(){
        return Dolar*Cotacao;
    }
    
}
