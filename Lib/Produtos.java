package Lib;

public class Produtos {
    
    private double Preco, percentual;
    private int Limite;
    private double TotalCusto=0, TotalVenda=0, MediaCusto=0, MediaVenda=0;
    private double[] PrecoDeCusto, ValorDeVenda, Margem;
    private String[] Nome, Status;

    public Produtos(int Limite){
        this.Limite = Limite;
        PrecoDeCusto = new double[Limite];
        ValorDeVenda = new double[Limite];
        Margem = new double[Limite];
        Nome = new String[Limite];
        Status = new String[Limite];
    }

    public Produtos(double Preco, double percentual){
        this.Preco = Preco;
        this.percentual = percentual;
    }

    public double Total(){
        return Preco+((Preco*percentual)/100);
    }

    public void Calcular(){

        for (int i = 0; i < Limite; i++) {

            Nome[i] = Prompt.lerLinha("Digite o nome do produto: ");
            PrecoDeCusto[i] = Prompt.lerDecimal("Digite o preço de custo do produto: ");
            ValorDeVenda[i] = Prompt.lerDecimal("Digite o preço de venda do produto: ");

            TotalCusto+=PrecoDeCusto[i];
            TotalVenda+=ValorDeVenda[i];

            Margem[i]=(ValorDeVenda[i]-PrecoDeCusto[i]);

            if (Margem[i]>0) {
                Status[i] = "lucro";
            } else {
                Status[i] = "prejuízo";
            }
            
        }

        Prompt.separador();

        for (int i = 0; i < Limite; i++) {
            Prompt.imprimir("O produto '" + Nome[i] + "' deu " + Status[i] + " de aprocimadamente " + Margem[i]);
        }

        MediaCusto=TotalCusto/Limite;
        MediaVenda=TotalVenda/Limite;

        Prompt.imprimir("o valor medio do preço de custo é " + MediaCusto);
        Prompt.imprimir("o valor medio do preço de venda é " + MediaVenda);

    }

}