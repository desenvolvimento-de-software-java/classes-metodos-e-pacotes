package Lib;

public class Triangulo {
    private double LadoA, LadoB, LadoC;

    public Triangulo(double LadoA, double LadoB, double LadoC){
        this.LadoA = LadoA;
        this.LadoB = LadoB;
        this.LadoC = LadoC;
    }

    public boolean TrianguloVar(){

        if ((LadoA+LadoB)>LadoC){
            return true;
        }

        else if ((LadoB+LadoC)>LadoA){
            return true;
        }

        else if ((LadoA+LadoC)>LadoB){
            return true;
        }

        else{
            return false;
        }

    }

    public String Tipo(){

        if(LadoA==LadoB && LadoA==LadoC){
            return "Equilátero";
        }

        else if (LadoA==LadoB) {
            return "Isóscele";
        }

        else if (LadoB==LadoC) {
            return "Isóscele";
        }

        else if (LadoA==LadoC) {
            return "Isóscele";
        }
        
        else{
            return "Escaleno";
        }

    }
    
}
