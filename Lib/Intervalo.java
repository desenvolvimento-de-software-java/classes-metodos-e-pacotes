package Lib;

public class Intervalo {
    private int Limite, SexoM=0, SexoF=0;
    private int[] numeros;
    private String[] Sexo, Nomes;

    public Intervalo(int Limite) {
        this.Limite = Limite;
        numeros = new int[Limite];
        Nomes = new String[Limite];
        Sexo = new String[Limite];
    }

    public void fornum(){

        for (int i = 0; i < Limite; i++) {
            numeros[i] = Prompt.lerInteiro("Digite um número para saber se ele esta no intervalo de 10 a 150:");
        }

        Prompt.imprimir("Os numeros no intervalo de 10 a 150 sao:");

        for (int i = 0; i < Limite; i++) {

            if (numeros[i] >= 10 && numeros[i] <= 150) {
               Prompt.Print(numeros[i] + ", ");
            }
        }
    }

    public void NomesEIdades(){

        for (int i = 0; i < Limite; i++) {
            Nomes[i] = Prompt.lerLinha("Digite o nome:");
            numeros[i] = Prompt.lerInteiro("Digite a idade:");
        }

        for (int i = 0; i < Limite; i++) {

            if (numeros[i]>=18) {
                Prompt.imprimir(Nomes[i] + " é maior de idade");
            }
            else{
                Prompt.imprimir(Nomes[i] + " é menor de idade");
            }
        }
    }
    
    public void NomesESexo(){

        Sexo Sexo_ = new Sexo();

        for (int i = 0; i < Limite; i++) {
            Nomes[i] = Prompt.lerLinha("Digite o nome:");

            Sexo[i] = Sexo_.VeriSexo();

            SexoM += Sexo_.GetSexoM();
            SexoF += Sexo_.GetSexoF();

        }

        for (int i = 0; i < Limite; i++) {
            Prompt.imprimir(Nomes[i] + " é do sexo " + Sexo[i]);
        }

        Prompt.imprimir("O totas de pessoas com o sexo masculino é " + SexoM);
        Prompt.imprimir("O totas de pessoas com o sexo Feminino é " + SexoF);
    }
}