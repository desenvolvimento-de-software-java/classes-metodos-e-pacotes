/*

26. Uma seguradora possui nove categorias de seguro baseadas na idade e ocupação do segurado. 
Somente pessoas com pelo menos 17 anos e não mais que 70 anos podem adquirir apólices de 
seguro. Quanto às classes de ocupações, foram definidos três grupos de risco: baixo, médio e alto. 
A tabela abaixo fornece as categorias em função da faixa etária e do grupo de risco. Dados nome, 
idade e grupo de risco de um pretendente, determinar e imprimir seus dados e categoria. Caso a 
idade não esteja na faixa necessária, imprimir uma mensagem informando que ele não se enquadra 
em nenhuma das categorias ofertadas. 

                  Grupo de Risco 
    Idade      Baixo   Médio   Alto 
    17 a 20      1       2       3 
    21 a 24      2       3       4 
    25 a 34      3       4       5 
    35 a 64      4       5       6 
    65 a 70      7       8       9 

*/

import Lib.*;

public class Exercicio26 {
    public static void Executar(){

        boolean Var;
        int Risco;

        String Nome = Prompt.lerLinha("Digite o nome");

        int Idade = Prompt.lerInteiro("Digite a idade");

        do {
            Var=false;

            Prompt.imprimir("B - Baixo  /  M - Médio  /  A - Alto");

            String Tipo = Prompt.lerLinha("Digite o grupo de risco");

            Risco = Grupo.Risco(Idade, Tipo);

            if (Risco==0) {
                Var=true;
            }

        } while (Var==true);

        if (Idade>=17 && Idade<=70) {

            Prompt.imprimir(Nome + " se encontra na nivel de risco igual a " + Risco);

        } else {

            Prompt.imprimir("não se enquadra em nenhuma das categorias ofertadas");

        }
        
    }
}