/*

2. Escrever um programa para determinar o consumo médio de um automóvel sendo fornecida a 
distância total percorrida pelo automóvel e o total de combustível gasto. 
 
*/

import Lib.*;

public class Exercicio02 {

    public static void Executar(){

        double Distancia = Prompt.lerDecimal("Digite a distancia percorrida:");

        double Gasto = Prompt.lerDecimal("Digite o total de gasolina gasto:");

        Carro Carro = new Carro(Distancia, Gasto);

        Prompt.imprimir("O consumo médio do carro é de " + Carro.Media() + "KM por litro.");
    }
}