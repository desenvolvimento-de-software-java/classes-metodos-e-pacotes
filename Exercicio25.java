/*

25. Dado o nome de um estudante, com o respectivo número de matrícula e as três notas acima 
mencionadas,  desenvolva  um  programa  para  calcular  a  nota  final  e  a  classificação  de  cada 
estudante. A classificação é dada conforme a lista abaixo: 
Nota Final Classificação.
    [8,10] A 
    [7,8] B 
    [6,7] C 
    [5,6] D 
    [0,5] R 
    Imprima o nome do estudante, com o seu número, nota final e classificação.

*/

import Lib.*;

public class Exercicio25 {
    public static void Executar(){

        String Nome = Prompt.lerLinha("Digite o nome do aluno");

        int RGM = Prompt.lerInteiro("Digite o RGM do aluno");

        double Laboratorio = Prompt.lerDecimal("Digite a nota de laboratorio");

        double Avaliacao = Prompt.lerDecimal("Digite a nota da avaliaçao");

        double Final = Prompt.lerDecimal("Digite a nota da prova final");

        double Total = Aluno.CalculoNota(Laboratorio, Avaliacao, Final);

        String NotaL = Aluno.Letra(Laboratorio, Avaliacao, Final);

        Prompt.imprimir("O aluno " + Nome + " portador do RGM " + RGM + " possui notal final de " + Total + " e esta classificado com categoria'" + NotaL + "'");
        


        
    }
}