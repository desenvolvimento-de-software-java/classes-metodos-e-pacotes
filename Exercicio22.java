/*

22. Faça um programa que calcule o valor da conta de luz de uma pessoa. 
Sabe-se que o cálculo da conta de luz segue a tabela abaixo: 
    Tipo de Cliente Valor do KW/h 
    1 (Residência) 0,60 
    2 (Comércio) 0,48 
    3 (Indústria) 1,29

*/

import Lib.*;

public class Exercicio22 {
    public static void Executar(){

        boolean Var;

        Prompt.imprimir("1 - Residência / 2 - Comércio / 3 - Indústria");

        do {

            Var=false;

            int Tipo = Prompt.lerInteiro("Digite qual o tipo da residnecia");
    
            double Kwh = Prompt.lerDecimal("Digite o todal de kw/h consumido.");
    
            double Total = Conta.CalculoConta(Tipo, Kwh);

            if (Total!=0) {
                Prompt.imprimir("O total a ser pago é de R$" + Total);
            }

            else{
                Prompt.imprimir("Tipo incorreto.");
                Prompt.separador();
                Var=true;
            }
            
        } while (Var==true);
        
    }
}