/*

14. Faça um programa que receba o preço de custo e o preço de venda de 40 produtos. Mostre 
como resultado se houve lucro, prejuízo ou empate para cada produto. Informe média de preço de 
custo e do preço de venda.

*/

import Lib.*;

public class Exercicio14 {
    public static void Executar(){

        int Limite = Prompt.lerInteiro("Digite quantos produtos voce quer analizar: ");

        Produtos Produtos = new Produtos(Limite);

        Produtos.Calcular();
        
    }
}