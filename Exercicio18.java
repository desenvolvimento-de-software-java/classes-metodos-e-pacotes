/*

18. Faça um programa que receba o nome a idade, o sexo e salário fixo de um funcionário. Mostre 
o nome e o salário líquido acrescido do abono conforme o sexo e a idade:
    Sexo Idade Abono 
    M >= 30    100,00 
    M <  30    50,00 
    F >= 30    200,00 
    F <  30    80,00 

*/

import Lib.*;

public class Exercicio18 {
    public static void Executar(){

        int Limite = Prompt.lerInteiro("Digite quantos funcionario voce quer analizar: ");

        Empresa Empresa = new Empresa(Limite);

        Empresa.AjustSalario();
        
    }
}