/*

11. Escrever um programa que leia o nome e o sexo de 56 pessoas e informe o nome e se ela é 
homem ou mulher. No final informe total de homens e de mulheres.

*/

import Lib.*;

public class Exercicio11 {
    public static void Executar(){

        int Limit = Prompt.lerInteiro("Digite quantos pessoas voce que analizar:");

        Intervalo Intervalo = new Intervalo(Limit);

        Intervalo.NomesESexo();
        
    }
}