/*

21. Elabore um programa que, dada a idade de um nadador. Classifique-o em uma das seguintes 
categorias: 
    Infantil A = 5 - 7 anos 
    Infantil B = 8 - 10 anos 
    juvenil A = 11- 13 anos 
    juvenil B = 14 - 17 anos 
    Sênior = 18 - 25 anos 
    Apresentar mensagem “idade fora da faixa etária” quando for outro ano não contemplado.

*/

import Lib.*;

public class Exercicio21 {
    public static void Executar(){

        boolean Var;

        do {
            Var=false;

            String Nome = Prompt.lerLinha("Digite o nome do nadador: ");

            int Idade = Prompt.lerInteiro("Digite a idade do nadador: ");

            Prompt.imprimir("O nadador " + Nome + " esta classificado como " + Natacao.classificacao(Idade));

            String Veri = Prompt.lerLinha("Deseja tentar novamente?");

            Natacao.Veri(Veri);

        } while (Var==true);
        
    }
}