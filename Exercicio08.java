/*

8. Escrever um programa que leia o nome e as três notas obtidas por um aluno durante o semestre. 
Calcular  a  sua  média  (aritmética),  informar  o  nome  e  sua  menção  aprovado  (media  >=  7), 
Reprovado (media <= 5) e Recuperação (media entre 5.1 a 6.9).

*/

import Lib.*;

public class Exercicio08 {
    public static void Executar(){

        String Nome = Prompt.lerLinha("Digite o nome do aluno:");

        double Nota1 = Aluno.VerificarNota("Digite a primeira nota");
        double Nota2 = Aluno.VerificarNota("Digite a segunda nota");
        double Nota3 = Aluno.VerificarNota("Digite a terceira nota");

        Aluno Aluno = new Aluno(Nome, Nota1, Nota2, Nota3);

        Aluno.menção();
        
    }
}