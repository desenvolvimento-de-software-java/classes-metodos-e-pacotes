/*

6. Faça um programa que receba o preço de custo de um produto e mostre o valor de venda. Sabe-
se  que  o  preço  de  custo  receberá  um  acréscimo  de  acordo  com  um  percentual  informado  pelo 
usuário.

*/

import Lib.*;

public class Exercicio06 {
    public static void Executar(){

        double Preco = Prompt.lerDecimal("Digite o preço de custo do produto: ");

        double percentual = Prompt.lerDecimal("Digite o percentual de lucro para acrecentar ao produto:");

        Produtos Produtos = new Produtos(Preco, percentual);

        Prompt.imprimir("O valor total do produto ja com o percentual é " + Produtos.Total());
        
    }
}