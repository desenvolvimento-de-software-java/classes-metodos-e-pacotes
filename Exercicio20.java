/*

20. A escola “APRENDER” faz o pagamento de seus professores por hora/aula. Faça um programa 
que calcule e exiba o salário  de um professor. Sabe-se que o  valor da hora/aula segue a tabela 
abaixo: 
    Professor Nível 1 R$12,00 por hora/aula 
    Professor Nível 2 R$17,00 por hora/aula 
    Professor Nível 3 R$25,00 por hora/aula 

*/

import Lib.*;

public class Exercicio20 {
    public static void Executar(){

        double Total;

        String Nome = Prompt.lerLinha("Digite o nome do professor: ");

        do {

            double Horas = Prompt.lerDecimal("Digite o total de horas trabalhadas pelo professor: ");

            Prompt.imprimir("Nível 1  /  Nível 2  /  Nível 3");

            int Nivel = Prompt.lerInteiro("Digite o nivel de aula desse professor: ");

            Total = Empresa.SalarioProfessor(Horas, Nivel);
            
        } while (Total==0);

        Prompt.imprimir("O salario do professor(a) " + Nome + " é de " + Total);

    }
}