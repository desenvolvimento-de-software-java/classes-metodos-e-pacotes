/*

24. Em um curso de Ciência da Computação a nota do estudante é calculada a partir de três notas 
atribuídas, respectivamente, a um trabalho de laboratório, a uma avaliação semestral e a um exame 
final. As notas variam, de 0 a 10 e a nota final é a média ponderada das três notas mencionadas. 
    A lista abaixo fornece os pesos: 
    a. Laboratório: peso 2 
    b. Avaliação semestral: peso 3 
    c. Exame final: peso 5

*/

import Lib.*;

public class Exercicio24 {
    public static void Executar(){

        String Nome = Prompt.lerLinha("Digite o nome do aluno");

        double Laboratorio = Prompt.lerDecimal("Digite a nota de laboratorio");

        double Avaliacao = Prompt.lerDecimal("Digite a nota da avaliaçao");

        double Final = Prompt.lerDecimal("Digite a nota da prova final");

        double Total = Aluno.CalculoNota(Laboratorio, Avaliacao, Final);

        Prompt.imprimir("A nota final do anuno " + Nome + " é de " + Total);
        
    }
}