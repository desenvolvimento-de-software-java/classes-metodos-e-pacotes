/*

1. Escrever um programa que leia o nome de um aluno e as notas das três provas que ele obteve 
no semestre. No final informar o nome do aluno e a sua média (aritmética). 

*/

import Lib.*;

public class Exercicio01 {

    public static void Executar(){

        String Nome = Prompt.lerLinha("Digite o nome do aluno");

        double Nota1 = Aluno.VerificarNota("Digite a primeira nota");
        double Nota2 = Aluno.VerificarNota("Digite a segunda nota");
        double Nota3 = Aluno.VerificarNota("Digite a terceira nota");

        Aluno Aluno = new Aluno(Nome, Nota1, Nota2, Nota3);

        Prompt.imprimir("O nome do aluno é " + Aluno.getNome());
        Prompt.imprimir("A média do aluno é " + Aluno.calcularMedia());
    }
}